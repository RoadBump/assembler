/*
 * hashtable.c
 * A generic hash table that can store objects of any type
 *
 *  Created on: 21\7\2014
 *      Author: Joseph Bruss
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"

#define INIT_SIZE 32
#define LOAD_TRESHOLD 1

extern void *critimalloc(size_t size);
extern void *criticalloc(size_t count, size_t eltsize);

/*
 * Implementation: an array of buckets, each bucket points to linked list
 * of Entry structs, sorted by key.
 */

/*
 * Returns hash code for specified key and size
 */
static int get_hash(char *key, int size)
{
	/*djb2 algorithm*/
#define DJB_INIT 5381
	unsigned int hash = DJB_INIT;
	int c;

	while ((c = *key++))
	    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */

	return hash % size;
}

/*
 * Inserts node into the linked list whose head is it's head, sorted by key. Fails on duplicate keys,
 * Returns the new head of the list, or NULL if failed.
 */
static Entry *insert(Entry *node, Entry *head)
{
	Entry *prev=NULL,
		*current=head;
	int cmp=1;

	while(current!=NULL && (cmp=strcmp(node->key,current->key))>0)
	{
		prev=current;
		current=current->next;
	}

	if(cmp==0)/*duplicate key*/
		head=NULL;
	else /*insert into list*/
	{
		node->next=current;
		if(prev==NULL)
			head=node;
		else
			prev->next=node;
	}

	return head;
}

/*
 * Deletes the entry with specified key from a sorted linked list.
 * Returns the new list head after deletion. Stores deleted entry in deleted, or NULL if not found.
 */
static Entry *delete(char *key, Entry *head, Entry **deleted)
{
	Entry *prev=NULL,
			*current=head;
	int cmp=1;

	while(current!=NULL && (cmp=strcmp(key,current->key))>0)
	{
		prev=current;
		current=current->next;
	}

	if(cmp==0)/*found*/
	{
		if(prev==NULL)/*delete head*/
			head=current->next;
		else
			prev->next=current->next;

		current->next=NULL;
		*deleted=current;
	}
	else
		*deleted=NULL;

	return head;
}

/*
 * Returns the entry that matches the key from within a sorted linked list, or NULL if not found.
 */
static Entry *get(char *key, Entry *head)
{
	Entry *node=head;
	int cmp=1;

	while(node!=NULL && (cmp=strcmp(key,node->key))>0)
		node=node->next;

	return (cmp==0?node:NULL);
}

/*
 * Enlarges array size, re-hashes all values accordingly
 */
static void rehash(Hashtable *ht)
{
	int new_size, i;
	Entry **new_array;

	new_size=ht->size==0?INIT_SIZE:ht->size*2;/*double the array size*/
	new_array=(Entry **)criticalloc(new_size, sizeof(Entry *));
	/*re-hash*/
	for(i=0;i<ht->size;i++)
	{
		Entry *entry, *next;
		entry=ht->array[i];
		while(entry)
		{
			next=entry->next;
			int hash=get_hash(entry->key, new_size);
			new_array[hash]=insert(entry,new_array[hash]);
			entry=next;
		}
	}

	if(ht->array!=NULL)
		free(ht->array);
	ht->array=new_array;
	ht->size=new_size;
}

/*
 * Adds a key/value pair to the hash table. Fails if key already exists.
 * Returns whether the operation succeeded.
 */
int ht_add(Hashtable *ht, char *key, void *value)
{
	if(ht && key)/*key can't be null*/
	{
		Entry *entry,*ret;
		/*ensure that the table is big enough*/
		if(ht->size==0
				|| (double)ht->count/ht->size > LOAD_TRESHOLD)/*table too loaded*/
			rehash(ht);

		entry=(Entry *)critimalloc(sizeof(Entry));
		entry->key=key;
		/*insert into array at index calculated by hash*/
		int hash=get_hash(key,ht->size);
		ret=insert(entry,ht->array[hash]);

		if(ret)/*success*/
		{
			ht->count++;
			entry->value=value;
			/*copy key string*/
			entry->key=(char *)critimalloc(strlen(key) + 1);
			strcpy(entry->key, key);
			ht->array[hash]=ret;/*list head*/
		}
		else
			free(entry);

		return (ret!=NULL);
	}
	return 0;
}

/*
 * Removes a key/value pair from hash table.
 * Returns the value that has been removed, or NULL if not exist.
 * Does NOT free any memory used by value.
 */
void *ht_remove(Hashtable *ht, char *key)
{
	if(ht && ht->size>0 && key)/*key cannot be null*/
	{
		int hash;
		Entry *deleted;

		hash=get_hash(key,ht->size);
		ht->array[hash]=delete(key,ht->array[hash],&deleted);
		if(deleted)
		{
			void *value=deleted->value;
			free(deleted->key);
			free(deleted);

			ht->count--;
			return value;
		}
	}
	return NULL;
}

/*
 * Returns the value that is associated with the key, or NULL if not exist.
 */
void *ht_get(Hashtable *ht, char* key)
{
	if(ht && ht->size>0 && key)
	{
		int hash=get_hash(key,ht->size);
		Entry *entry=get(key, ht->array[hash]);
		if(entry)
			return entry->value;
	}
	return NULL;
}

/*
 * Returns whether the key appears in the table.
 */
int ht_exists(Hashtable *ht, char *key)
{
	if(ht && ht->size>0 && key)
	{
		int hash=get_hash(key, ht->size);
		return get(key, ht->array[hash])!=NULL;
	}
	return 0;
}

/*
 * Returns the number of objects stored in hash table.
 */
int ht_count(Hashtable *ht)
{
	return ht->count;
}

/*
 * Iterates over all values in table, calling a supplied function for each of them.
 * First parameter is the key of current item, second parameter is it's value.
 * Iteration continues as long as func returns non-zero.
 */
void ht_enumerate(Hashtable *ht, int (*func)(char*, void*))
{
	if(ht && func)/*func is not null*/
	{
		/*call func for each item in array while it returns nonzero, passing the key and value*/
		char *tmp_key=NULL;
		int key_len=0;
		int i, contin;
		for(i=0, contin=1; contin && i<ht->size; i++)
		{
			Entry *entry=ht->array[i];
			while(contin && entry)
			{
				if(strlen(entry->key)>=key_len)
				{
					if(tmp_key)
						free(tmp_key);
					key_len=2*strlen(entry->key);/*double size*/
					tmp_key=(char *)critimalloc(key_len);
				}
				strcpy(tmp_key, entry->key);/*copy key for security*/

				contin=func(tmp_key, entry->value);
				entry=entry->next;
			}
		}

		if(tmp_key)
			free(tmp_key);
	}
}

/*clear the hash table, optionally freeing the values*/
void ht_clear(Hashtable *ht, int free_items)
{
	if(ht)
	{
		int i;
		for(i=0; i<ht->size; i++)
		{
			/*free linked list*/
			Entry *entry, *next=ht->array[i];
			while((entry=next))
			{
				free(entry->key);
				if(free_items && entry->value)
					free(entry->value);
				next=entry->next;
				free(entry);
			}
		}

		if(ht->array)
			free(ht->array);
		ht->array=NULL;
		ht->count=ht->size=0;
	}
}
