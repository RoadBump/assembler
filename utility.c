/*
 * utility.c
 * Various utility functions
 *
 *  Created on: 22/7/2014
 *      Author: Joseph Bruss
 */
#include <stdio.h>
#include <stdlib.h>

/*
 * Calls malloc to allocate memory in requested size. Terminates the program on failure.
 * Returns a pointer obtained by malloc.
 */
void *critimalloc(size_t size);

/*
 * Calls calloc to allocate memory. Terminates the program on failure.
 * REturns a pointer obtained by calloc.
 */
void *ctiticalloc(size_t count, size_t eltsize);

void *critimalloc(size_t size)
{
	void *p=malloc(size);
	if(!p)
	{
		fprintf(stderr,"Fatal error: not enough memory.");
		exit(1);
	}
	return p;
}

void *criticalloc(size_t count, size_t eltsize)
{
	void *p=calloc(count, eltsize);
	if(!p)
	{
		fprintf(stderr,"Fatal error: not enough memory.");
		exit(1);
	}
	return p;
}

/*
 * Enlarges given array, that was dynamically allocated, or not initialized at all.
 * Terminates the program on failure.
 * array- in and out parameter. contains the array to enlarge, receives the larger array.
 * size- number of elements currently in array
 * elt_size- size of an array element, in chars
 * init_size- initial array size, if array size is zero
 * Returns the number of elements in the larger array
 */
int grow_array(void **array_ptr, int size, int elt_size, int init_size)
{
	void *p;
	if(size == 0)
	{
		size=init_size;
		p=malloc(size*elt_size);
	}
	else/*doubles array size*/
	{
		size*=2;
		p=realloc(*array_ptr, size*elt_size);
	}

	if(!p)
	{
		fprintf(stderr, "%s", "Fatal error: not enough memory.");
		exit(1);
	}
	*array_ptr=p;
	return size;
}
