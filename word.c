/*
 * word.c
 * Representation of a target machine word, 20 bits wide.
 *
 *  Created on: 21/7/2014
 *  Author: Joseph Bruss
 */

#include <stdio.h>
#include <stdlib.h>
#include "word.h"

/* The 20 bit word is implemented using a big-enough integral type. defined in word.h.
 * Assuming there is any on the running system. If not, we can change the implementation
 * to a char[] or something else, without breaking the rest of the program.
 * A word is always two's complement. For compatibility with one's complement
 * machines, use an unsigned integer and manipulate bits manually.
 */
#define MASK 0xFFFFF;

/*
 * Parse a string of decimal digits with an optional sign to a Word. Store result in res.
 * Returns whether the conversion succeeded.
 * If the conversion failed, res is assigned WORD_MAX on overflow error and WORD_ZERO otherwise.
 */
int word_parse(char *s, Word *res)
{
	int sign;
	unsigned long val;
	/*skip and record sign*/
	if((sign=*s=='-')||*s=='+')
		s++;
	val=strtoul(s,&s,0);
	if(*s=='\0' && val<=WORD_MAX)/*conversion succeeded and no overflow*/
	{
		if(sign)
			/*negative number, use explicit 2's complement negation*/
			val= ~val + 1;
		*res=(Word)val & MASK;
		return 1;
	}
	else
	{
		*res=val > WORD_MAX ? WORD_MAX : WORD_ZERO;/*WORD_MAX on overflow, WORD_ZERO otherwise*/
		return 0;
	}
}

/*
 * Converts an integer to a Word. No check for overflow.
 */
Word word_from_int(int n)
{
	Word res;
	int sign=n<0;
	if(sign)
		n=-n;
	res=(Word)n;
	if(sign)
		res= ~res+1;/*two's complement negation*/
	res&=MASK;/*delete any extra bits*/
	return res;
}

/*
 * Prints the value of w in a seven-digit octal form into s, with leading zeros if necessary.
 */
void word_print(char *s, Word *w)
{
	sprintf(s,"%07lo",(unsigned long)*w);
}

/*
 * Returns a specified bit sequence of w, right justified.
 * index: the starting index, from the right edge
 * count: the number of bits to copy from right to left
 * index, count and index+count must all be in range 0-19. Count must also fit into an unsigned integer.
 */
unsigned word_get_bits(Word *w, int index, int count)
{
	/*create a mask with those bits on.*/
	Word mask=0;
	mask=~(~mask<<count);
	mask=mask<<index;
	/*obtain requested bits*/
	mask=mask&*w;
	/*justify to right. Word is unsigned, so right shifting fills with zeroes*/
	mask=mask>>index;
	return (unsigned)mask;
}

/*
 * Sets bits in w to a specified bit pattern, given by bits.
 * index: the starting bit index in w, from the right edge
 * count: the number of bits to copy.
 * bits: the unsigned integer from where the 'count' rightmost bits are copied.
 * index, count and index+count must all be in range 0-19. Count must also fit into an unsigned integer.
 */
void word_set_bits(Word *w, int index, int count, unsigned bits)
{
	Word mask, pattern;
	/*create a mask with those bits on*/
	mask = 0;
	mask= ~(~mask << count);
	mask <<= index;

	pattern = bits;
	pattern <<= index;
	/*turn off bits that are not copied*/
	bits &= mask;
	/*turn on all bits but those specified*/
	mask = ~mask;
	*w = (*w & mask) | pattern;
}

/*
 * Returns whether x and y are equal.
 */
int word_equals(Word x, Word y)
{
	return x==y;
}
