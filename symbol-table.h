/*
 * symbol-table.h
 *
 *  Created on: 4/8/2014
 *      Author: Joseph Bruss
 */

#ifndef SYMBOL_TABLE_H_
#define SYMBOL_TABLE_H_

/*Describes the type of a declared symbol*/
enum SymbolType {
	UNDEFINED,/*symbol not yet defined*/
	EXTERN,   /*external symbol*/
	CODE,     /*symbol declared in an instruction line*/
	DATA,     /*symbol declared in .data or .string line*/
	OTHER     /*symbol declared but cannot be referenced, e.g. in .entry directive*/
};

/*Addressing modes of operands*/
enum AddrMode {
	VALUE = 0,	/*Operand holds a value*/
	SYMBOL = 1,	/*Operand is a symbol reference*/
	INDEX = 2,	/*Operand is a symbol reference, with an index*/
	REG = 3		/*Operand is a register*/
};

/*
 * Adds a local symbol to symbol table.
 * label- the symbol
 * address- address in code or data block to replace with symbol value. in use only when type is CODE or DATA
 * size- size of labeled instruction. in use only when type is CODE.
 * type- symbol type. must be CODE, DATA, or OTHER.
 */
void add_symbol(char *label, int address, int size, int type);

/*
 * Adds a reference to a symbol.
 * ref_address- address where to place actual symbol value
 * line_number- line number in code where the reference appears
 * is_index- is it an index reference or not
 */
void add_ref(char *label, int ref_address, int line_number, int is_index);

/*Adds an external symbol (i.e. declared in a '.extern' directive)*/
void add_extern(char *label);

/*Declares a symbol as an 'entry'*/
void add_entry(char *label);

/*Resolves all symbols, updating references to actual values*/
void resolve_symbols(void);

/*Clear the symbol table*/
void clear_symbols(void);

#endif /* SYMBOL_TABLE_H_ */
