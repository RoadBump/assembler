/*
 * assembler.c
 * Contains main assembler logic
 *
 *  Created on: 26\7\2014
 *      Author: Joseph Bruss
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"
#include "input-output.h"
#include "symbol-table.h"

#define MAX_TOKENS 20
#define MAX_LINE 150
#define MAX_INST_LINE 80
#define MAX_LABEL 30
/*Bit field constants that hold info about the validity of each addressing method*/
#define ADDR_VAL (1 << VALUE)
#define ADDR_SYMBOL (1 << SYMBOL)
#define ADDR_INDEX (1 << INDEX)
#define ADDR_REG (1 << REG)
#define ADDR_NOT_VAL (ADDR_SYMBOL | ADDR_INDEX | ADDR_REG)
#define ADDR_ALL (ADDR_NOT_VAL | ADDR_VAL)

/*An instruction operand*/
typedef struct {
	unsigned reg:3;				/*register number*/
	unsigned address_mode:2;	/*addressing mode*/
	Word words[2];		/*additional words, up to 2. Count and meaning are determined by addressing mode*/
	char *labels[2];	/*labels referenced by operand, up to 2. Count and meaning are determined by addressing mode*/
} Operand;

/*Holds instruction data*/
typedef struct {
	Operand dest;/*destination operand*/
	Operand source;/*source operand*/
	unsigned comb:2;
	unsigned opcode:4;
	unsigned type:1;
	unsigned dbl:1;
} Instruction;

/*Holds the expected state of an instruction*/
typedef struct {
	char *name;/*instruction name*/
	int n_operands;/*expected number of operands*/
	unsigned opcode:4;
	unsigned source_modes:4;/*valid addressing modes for source operand. combination of ADDR_... constants.*/
	unsigned dest_modes:4;/*valid addressing modes for destination operand. combination of ADDR_... constants.*/
} InstInfo;

typedef void (*dir_handler)(char *, char *[], int);

static Hashtable inst_table;/*Holds info about instructions*/
static Hashtable dir_table;/*Holds pointers to functions who handle directives*/

static void handle_instruction(char* label, char* tokens[], int n_tokens);
static int parse_type_dbl(char *s,Instruction *inst);
static int parse_operand(char *s, Operand *operand);
static int check_addr_mode(int valid_modes, int actual, int op_number);
static Word inst_to_word(Instruction *inst);
static void write_operand(Operand *operand);
static void handle_directive(char *label, char *tokens[], int n_tokens);
static void init_op_tables(void);
/*
 * Compiles current file.
 * Returns whether the compilation process succeeded.
 */
int compile(char* filename)
{
	char line[MAX_LINE];
	char* tokens[MAX_TOKENS];
	char* label;
	int n_tokens, line_length, ret;

	init_op_tables();

	if((ret = begin_source_file(filename)))
	{
		while((n_tokens = get_next_line(line, MAX_LINE, &label, tokens, MAX_TOKENS, &line_length)) >= 0)
		{
			if(label!=NULL && label!=line)
				set_error("label must start at the beginning of the line.");

			if(n_tokens>0)
			{
				/*first token is a directive, which starts with a dot (.), or an instruction, which does not*/
				if(*tokens[0]=='.')/*directive*/
					handle_directive(label, tokens, n_tokens);
				else/*instruction*/
				{
					if(line_length > MAX_INST_LINE)
						set_error("instruction line cannot be longer than %d characters.",MAX_INST_LINE);
					handle_instruction(label,tokens, n_tokens);
				}
			}
			else if(label!=NULL)/*no valid tokens, handle label*/
				add_symbol(label, 0, 0, OTHER);/*check for duplicate symbols etc.*/
		}

		resolve_symbols();
		clear_symbols();

		ret=!has_error();
		flush();
	}

	return ret;
}

/*Compiles instruction line*/
static void handle_instruction(char* label, char* tokens[], int n_tokens)
{
	Instruction inst={{0}};
	int address=get_code_counter();/*address of current instruction*/
	/*first token is operation name, followed by a slash, followed by type\dbl combination*/
	char *slash=strchr(tokens[0],'/');
	if(slash)
		*slash='\0';/*trim type/dbl part*/
	InstInfo *info=(InstInfo *)ht_get(&inst_table, tokens[0]);/*get operation info by name*/
	if(info && n_tokens-1 == info->n_operands)/*instruction found and number of operands matches*/
	{
		int valid, i;
		inst.opcode=info->opcode;

		if(slash)
				*slash='/';/*restore slash*/
		valid= parse_type_dbl(slash, &inst);

		/*parse operands and verify addressing modes*/
		i=1;
		if(info->n_operands>1)/*first operand is source operand*/
		{
			valid=(parse_operand(tokens[i], &inst.source)
					&& check_addr_mode(info->source_modes, inst.source.address_mode, i));
			i++;
		}

		if(valid && info->n_operands>0)/*destination operand*/
			valid=(parse_operand(tokens[i], &inst.dest)
					&& check_addr_mode(info->dest_modes, inst.dest.address_mode, i));

		if(valid)
		{
			/*line parsed with no errors, inst contains all necessary information*/
			write_code(inst_to_word(&inst), LM_ABS);/*write instruction word*/
			if(info->n_operands>1)
				write_operand(&inst.source);
			if(info->n_operands>0)
				write_operand(&inst.dest);
		}
	}
	else/*operation not found or wrong number of operands*/
	{
		if(!info)
			set_error("no such operation '%s'.",tokens[0]);
		else
			set_error("wrong number of operands: expected %d.", info->n_operands);
	}

	if(label)
		add_symbol(label, address, get_code_counter() - address, CODE);
}

static int parse_type_dbl(char *s,Instruction *inst)
{
	int ret=0;
	if(s)
	{
		/*expected formats: /0,^ or /1/^/^,^  (^ means 0 or 1) */
		char comb1, comb2, dbl;
		if(sscanf(s,"/0,%c",&dbl)==1 &&(dbl=='0'||dbl=='1'))
		{
			inst->type=0;
			inst->dbl=(dbl=='1');
			ret=1;
		}
		else if(sscanf(s,"/1/%c/%c,%c",&comb1, &comb2, &dbl)==3
				&&(comb1=='0'||comb1=='1')
				&&(comb2=='0'||comb2=='1')
				&&(dbl=='0'||dbl=='1'))
		{
			inst->type=1;
			/*comb1 is left digit of comb*/
			inst->comb=(comb1=='1')<<1;
			inst->comb|=(comb2=='1');
			inst->dbl=(dbl=='1');
			ret=1;
		}
		else
			set_error("type and dbl part is invalid.");
	}
	else
		set_error("type and dbl part is missing from operation.");

	return ret;
}

/*
 * Parses an instruction operand. determines its addressing mode.
 * Returns whether the parsing succeeded, or failed due to format error.
 */
static int parse_operand(char *s, Operand *operand)
{
	int ret=1;
	if(*s=='#')/*value*/
	{
		Word w;
		if((ret=word_parse(s+1, &w)))
		{
			operand->words[0]=w;
			operand->address_mode=VALUE;
		}
		else
		{
			/*value of w signals the error type*/
			if(word_equals(w, WORD_MAX))/*overflow*/
				set_error("too big number '%s'.", s+1);
			else
				set_error("wrong number '%s'.", s+1);
		}
	}
	else if(s[0]=='r' && s[1]>='0' && s[1]<='7' && s[2]=='\0')/*register*/
	{
		operand->address_mode=REG;
		operand->reg=s[1]-'0';
	}
	else
	{
		int len=strlen(s);
		char *c;
		if((c = strchr(s, '{')) && c[1] == '!' && s[len-1] == '}')/*is label/index reference. format is LABEL{!INDEX} */
		{
			operand->address_mode=INDEX;
			*c='\0';/*terminate label*/
			s[len-1]='\0';/*terminate index*/
			operand->labels[0]=s;/*label*/
			operand->labels[1]=c+2;/*index*/
		}
		else/*is label reference*/
		{
			operand->address_mode=SYMBOL;
			operand->labels[0]=s;
		}
	}
	return ret;
}

/*
 * Checks if addressing mode is valid for a particular instruction.
 * valid_modes may be a combination of ADDR_... constants
 * actual is actual addressing mode, one of AddrMode enum values
 * op_number is the operand number, for error reporting
 */
static int check_addr_mode(int valid_modes, int actual, int op_number)
{
	actual = 1 << actual;/*convert from numeric code to bit field*/
	if((actual & valid_modes) == 0)/*invalid mode*/
	{
		char *name;
		switch(actual)
		{
		case ADDR_VAL: name="literal value"; break;
		case ADDR_SYMBOL: name="label reference"; break;
		case ADDR_INDEX: name="label/index reference"; break;
		case ADDR_REG: name="register name"; break;
		default: name="invalid value";break;
		}

		set_error("operand #%d: cannot use %s in this operand.", op_number, name);
		return 0;
	}
	return 1;
}

/*Encodes instruction to machine code*/
static Word inst_to_word(Instruction *inst)
{
	Word w=WORD_ZERO;
	word_set_bits(&w,0,2,inst->comb);
	word_set_bits(&w,2,3,inst->dest.reg);
	word_set_bits(&w,5,2,inst->dest.address_mode);
	word_set_bits(&w,7,3,inst->source.reg);
	word_set_bits(&w,10,2,inst->source.address_mode);
	word_set_bits(&w,12,4,inst->opcode);
	word_set_bits(&w,16,1,inst->type);
	word_set_bits(&w,17,1,inst->dbl);

	return w;
}

/*Writes operand machine code, adds labels referenced by operand to symbol table*/
static void write_operand(Operand *operand)
{
	int address=get_code_counter(),
		line=get_line_number();

	switch(operand->address_mode)
	{
	case VALUE:
		/*write literal value, stored in words[0]*/
		write_code(operand->words[0],LM_ABS);
		break;
	case SYMBOL:
		/*record reference to symbol given in labels[0]*/
		add_ref(operand->labels[0],address,line,0);
		write_code(WORD_ZERO, LM_RELOC);/*place holder for actual address*/
		break;
	case INDEX:
		/*two additional words, symbol and index. Record references, add place holders*/
		add_ref(operand->labels[0],address,line,0);/*symbol*/
		write_code(WORD_ZERO, LM_RELOC);
		add_ref(operand->labels[1],get_code_counter(),line,1);/*index*/
		write_code(WORD_ZERO,LM_ABS);
		break;
		/*register addressing requires no additional words*/
	}
}

/*Handles directive line*/
static void handle_directive(char *label, char *tokens[], int n_tokens)
{
	/*first token is directive type*/
	dir_handler handler=(dir_handler)ht_get(&dir_table, tokens[0]);/*get handler function by name*/
	if(handler)
		handler(label, tokens+1, n_tokens-1);/*omit first token*/
	else/*no such directive*/
		set_error("invalid directive '%s'.",tokens[0]);
}

/*Returns whether the given label is valid. Prints error if not.*/
int check_label(char *label)
{
	/*
	 * valid label:
	 * starts with an alphabetic character
	 * contains only alphanumeric characters
	 * length <= 30 characters
	 * is not an operation name or register name
	 */
	int valid = 1;

	if(!(valid=(label && isalpha(label[0]))))
		set_error("invalid label '%s': must start with an alphabetic character.", label);

	if(valid && strlen(label)>MAX_LABEL)
	{
		set_error("too long label, may contain at most %d characters.", MAX_LABEL);
		valid=0;
	}

	if(valid)
	{
		char *s=label;
		while(*++s)
			if(!isalnum(*s))/*non-alphanumeric character*/
			{
				set_error("invalid label '%s': contains non-alphanumeric character.", label);
				valid=0;
				break;
			}
	}

	if(valid)
		if((label[0]=='r' && label[1]>='0' && label[1]<= '7' &&label[2]=='\0') /*register name*/
							|| ht_exists(&inst_table, label)/*instruction name*/
							|| ht_exists(&dir_table, label))/*directive name*/
			{
				set_error("invalid label '%s': may not be a register or operation name.", label);
				valid=0;
			}

	return valid;
}

/*Directive handlers*/

/*Handles data directive*/
static void data_directive(char *label, char *tokens[], int n_tokens)
{
	/*input is expected to be a list of numbers, add them to data block*/
	int i;
	Word w;

	if(label)
		add_symbol(label, get_data_counter(), 0, DATA);

	for(i=0; i<n_tokens; i++)
		if(word_parse(tokens[i], &w))
			write_data(w);
		else
		{
			/*parse failed, reason is determined by the value of w*/
			if(word_equals(w, WORD_MAX))/*signal for overflow*/
				set_error("too big number '%s'.", tokens[i]);
			else
				set_error("wrong number '%s'.", tokens[i]);
			break;
		}
}

/*Handles string directive*/
static void string_directive(char *label, char *tokens[], int n_tokens)
{
	/*expected one token that contains a string, add to data block*/
	if(label)
		add_symbol(label, get_data_counter(), 0, DATA);

	if(n_tokens==1)
	{
		int len=strlen(tokens[0]);
		if(tokens[0][0]=='\"' && tokens[0][len-1]=='\"'
				&&len > 1)/*string contains at least 2 characters, starts and ends with " */
		{
			int i;
			for(i=1; i<len-1; i++)/*exclude qoutes*/
				write_data(word_from_int(tokens[0][i]));
			write_data(WORD_ZERO);
		}
		else
			set_error("string constant must be surrounded with double quotes.");
	}
	else
	{
		if(n_tokens==0)
			set_error("a string constant is expected.");
		else
			set_error("multiple string declared in single line.");
	}
}

/*Handles entry directive*/
static void entry_directive(char *label, char *tokens[], int n_tokens)
{
	/*declare specified symbol as entry*/
	if(label)/*label in entry directive is meaningless*/
		add_symbol(label, 0, 0, OTHER);

	if(n_tokens==1)
		add_entry(tokens[0]);
	else
	{
		if(n_tokens==0)
			set_error("no label specified in entry directive.");
		else
			set_error("only one label might appear in entry directive.");
	}
}

/*Handles extern directive*/
static void extern_directive(char *label, char *tokens[], int n_tokens)
{
	/*declare specified symbol as extern*/
	if(label)/*meaningless*/
		add_symbol(label, 0, 0, OTHER);

	if(n_tokens==1)
		add_extern(tokens[0]);
	else
	{
		if(n_tokens==0)
			set_error("no symbol specified in extern directive.");
		else
			set_error("only one symbol might be declared in extern directive.");
	}
}

#define MOV_CODE 0
#define CMP_CODE 01
#define ADD_CODE 02
#define SUB_CODE 03
#define NOT_CODE 04
#define CLR_CODE 05
#define LEA_CODE 06
#define INC_CODE 07
#define DEC_CODE 010
#define JMP_CODE 011
#define BNE_CODE 012
#define RED_CODE 013
#define PRN_CODE 014
#define JSR_CODE 015
#define RTS_CODE 016
#define STOP_CODE 017

/*Populates instruction table with info of all instructions*/
static void init_op_tables(void)
{
	if(ht_count(&inst_table)==0)
	{
		static InstInfo inst[]=
						{
								{"mov", 2, MOV_CODE, ADDR_ALL, ADDR_NOT_VAL},
								{"cmp", 2, CMP_CODE, ADDR_ALL, ADDR_ALL},
								{"add", 2, ADD_CODE, ADDR_ALL, ADDR_NOT_VAL},
								{"sub", 2, SUB_CODE, ADDR_ALL, ADDR_NOT_VAL},
								{"not", 1, NOT_CODE, 0, ADDR_NOT_VAL},
								{"clr", 1, CLR_CODE, 0, ADDR_NOT_VAL},
								{"lea", 2, LEA_CODE, ADDR_NOT_VAL, ADDR_NOT_VAL},
								{"inc", 1, INC_CODE, 0, ADDR_NOT_VAL},
								{"dec", 1, DEC_CODE, 0, ADDR_NOT_VAL},
								{"jmp", 1, JMP_CODE, 0, ADDR_NOT_VAL},
								{"bne", 1, BNE_CODE, 0, ADDR_NOT_VAL},
								{"red", 1, RED_CODE, 0, ADDR_NOT_VAL},
								{"prn", 1, PRN_CODE, 0, ADDR_ALL},
								{"jsr", 1, JSR_CODE, 0, ADDR_SYMBOL},
								{"rts", 0, RTS_CODE, 0, 0},
								{"stop", 0, STOP_CODE, 0, 0}
						};
		int i;
		for(i=0; i<(sizeof inst / sizeof inst[0]); i++)
			ht_add(&inst_table, inst[i].name, &inst[i]);
	}
	if(ht_count(&dir_table)==0)
	{
		ht_add(&dir_table, ".data", data_directive);
		ht_add(&dir_table, ".string", string_directive);
		ht_add(&dir_table, ".entry", entry_directive);
		ht_add(&dir_table, ".extern", extern_directive);
	}
}
