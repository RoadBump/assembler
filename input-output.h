/*
 * input-output.h
 *
 *  Created on: 4/8/2014
 *      Author: Joseph Bruss
 */

#ifndef INPUT_OUTPUT_H_
#define INPUT_OUTPUT_H_

#include "word.h"

/*Describes linking mode for a compiled word*/
enum LinkMode {
	LM_ABS = 'a',		/*absolute*/
	LM_RELOC = 'r',		/*relocatable*/
	LM_EXTERN = 'e'		/*external*/
};

/*
 * Reads next valid code line, splits it to tokens. Skips comment and blank lines. Checks for malformed lines.
 * Returns the number of tokens found, not including the label, or -1 on file error or end of file.
 * line- buffer to fill with characters read from file. All returned pointers point to within this buffer.
 * max_line- max line length
 * label- output parameter. Receives the label of the line, or NULL if not exist.
 * tokens- Array that is populated with all tokens in line, except the label. If tokens are malformed, remains empty.
 * max_tokens- size of array tokens.
 * length- output parameter. Receives the line length.
 */
int get_next_line(char *line, int max_line, char** label, char* tokens[], int max_tokens, int* length);

/*
 * Writes the formatted error message to stderr, and associates it with the current text line
 * Stops further writing to output.
 */
void set_error(char* format,...);

/*
 * Writes the formatted error message to stderr, and associates it with given line.
 * Stops further writing to output.
 * Set line_number to zero to mark general errors.
 */
void set_error_line(int line_number, char* format, ...);

/*
 * Returns whether an error occurred during current file compilation.
 */
int has_error(void);

/*
 * Writes code and link mode to the end code block.
 */
void write_code(Word word, char link_mode);

/*
 * Updates code block at given address. Address must correspond to that returned by get_code_counter().
 */
void update_code(int address, Word word, char link_mode);

/*
 * Writes data to the end of data block.
 */
void write_data(Word data);

/*
 * Writes symbol and address to externals file.
 */
void write_extern(char* symbol, int address);

/*
 * Writes symbol and address to entries file.
 */
void write_entry(char* symbol, int address);

/*
 * Begins input and output session for the specified file path.
 * Returns whether the file has opened successfully.
 */
int begin_source_file(char* file);

/*
 * Flushes all output to files, finishing input/output session.
 */
void flush(void);

/*
 *Returns the code counter
 */
int get_code_counter(void);

/*
 * Returns the data counter
 */
int get_data_counter(void);

/*
 * Returns current line number
 */
int get_line_number(void);

/*
 * Returns offset of code block in memory
 */
int get_code_offset(void);

/*
 * Returns offset of data block in memory
 */
int get_data_offset(void);

#endif /* INPUT_OUTPUT_H_ */
