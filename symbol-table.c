/*
 * symbol-table.c
 * Contains symbol table management
 *
 *  Created on: 31/7/2014
 *      Author: Joseph Bruss
 */

#include <stdio.h>
#include <stdlib.h>
#include "hashtable.h"
#include "input-output.h"
#include "symbol-table.h"

extern int check_label(char *label);
extern void *critimalloc(size_t size);
extern void *criticalloc(size_t count, size_t eltsize);
static int handle_symbol(char *label, void *sym);

/*Info about a reference to a symbol from machine code*/
typedef struct Ref {
	int address;			/*address of the word to be replaced by the symbol*/
	int is_index;			/*if only the size of the symbol is required*/
	int line_number;		/*line number in code where the reference appears*/
	struct Ref *next;
} Ref;

/*Holds information about a symbol*/
typedef struct{
	int address;			/*address of instruction, or data block, labeled with symbol.*/
	 	 	 	 	 	 	/*in use only when type is set to CODE or DATA*/
	int size;				/*size of instruction labeled with symbol*/
							/*in use only when type is set to CODE*/
	unsigned type:3;		/*type of symbol- one of SymbolType values*/
	unsigned is_entry:1;	/*if symbol is target of .entry directive*/
	Ref *refs_head;			/*linked list of references to the symbol*/
} Symbol;

static Hashtable symbol_table;

/*Adds a local symbol to symbol table.*/
void add_symbol(char *label, int address, int size, int type)
{
	if(check_label(label) && type != UNDEFINED && type !=EXTERN)/*add locally defined symbol only*/
	{
		/*check if symbol already exists*/
		Symbol *sym=(Symbol *)ht_get(&symbol_table, label);
		if(!sym)
		{
			/*add new symbol*/
			sym=criticalloc(1, sizeof(Symbol));
			sym->type=UNDEFINED;
			if(!ht_add(&symbol_table,label,sym))/*try to add*/
			{
				free(sym);
				sym=NULL;
			}
		}

		if(sym)
		{
			if(sym->type != UNDEFINED)/*duplicate symbol definition*/
			{
				if(sym->type == EXTERN)
					set_error("label '%s' already declared as external label.", label);
				else
					set_error("label '%s' defined more than once.", label);
			}
			else if(sym->is_entry && type == OTHER)
				/*symbol declared as entry must point to code or data*/
				set_error("symbol '%s' is declared as entry and must point to valid code or data.", label);
			else
			{
				/*symbol was added previously only for reference, now it is defined*/
				sym->address=address;
				sym->size=size;
				sym->type=type;
			}
		}
	}
}

/*Adds a reference to a symbol*/
void add_ref(char *label, int ref_address, int line_number, int is_index)
{
	if(check_label(label))
	{
		Symbol *sym=(Symbol *)ht_get(&symbol_table, label);
		if(!sym)
		{
			/*symbol does not exist, add a "not defined yet" symbol*/
			sym=(Symbol *)criticalloc(1, sizeof(Symbol));
			sym->type=UNDEFINED;

			if(!ht_add(&symbol_table, label, sym))/*try to add*/
			{
				free(sym);
				sym=NULL;
			}
		}

		if(sym)
		{
			Ref *ref=(Ref *)critimalloc(sizeof(Ref));
			ref->address=ref_address;
			ref->is_index=is_index;
			ref->line_number=line_number;
			ref->next=NULL;

			/*add to reference list*/
			if(!sym->refs_head)
				sym->refs_head=ref;
			else
			{
				Ref *node=sym->refs_head;
				while(node->next)
					node=node->next;
				node->next=ref;
			}
		}
	}
}

/*Adds an external symbol (i.e. declared in a '.extern' directive)*/
void add_extern(char *label)
{
	if(check_label(label))
	{
		Symbol *sym=(Symbol *)ht_get(&symbol_table, label);
		if(!sym)/*symbol does not exist, add new*/
		{
			sym=(Symbol *)criticalloc(1, sizeof(Symbol));
			if(!ht_add(&symbol_table, label, sym))/*try to add*/
			{
				free(sym);
				sym=NULL;
			}
		}

		if(sym)
		{
			if(sym->type != UNDEFINED && sym->type != EXTERN)
				/*defined as local symbol. multiple extern declarations are allowed.*/
				set_error("cannot declare '%s' as external symbol since it was defined as local symbol.", label);
			else if(sym->is_entry)
				/*external symbol cannot be an entry*/
				set_error("cannot declare '%s' as external symbol since it was declared as entry.", label);
			else
				sym->type=EXTERN;
		}
	}
}

/*Declares a symbol as an 'entry'*/
void add_entry(char *label)
{
	if(check_label(label))
	{
		Symbol *sym=(Symbol *)ht_get(&symbol_table, label);
		if(!sym)
		{
			sym=(Symbol *)criticalloc(1, sizeof(Symbol));
			if(!ht_add(&symbol_table, label, sym))/*try to add*/
			{
				free(sym);
				sym=NULL;
			}
		}

		if(sym)
		{
			if(sym->type == EXTERN)/*external symbol cannot be declared as entry*/
				set_error("symbol '%s' is declared as external and cannot be an entry.", label);
			else if(sym->type == OTHER)/*meaningless symbol, cannot be an entry*/
				set_error("symbol '%s' does not point to valid code or data and cannot be an entry.", label);
			else
				sym->is_entry=1;
		}
	}
}

/*Resolves all symbols, updating references to actual values*/
void resolve_symbols(void)
{
	/*pass on each symbol and resolve references*/
	ht_enumerate(&symbol_table, handle_symbol);
}

/*Clear the symbol table*/
void clear_symbols(void)
{
	ht_clear(&symbol_table, 1);
}

/*
 * Finishes the job with a symbol, updates all references, including .extern and .entry directives.
 * Is called for each symbol in table, in turn.
 * val is expected to point to a symbol
 */
static int handle_symbol(char *label, void *val)
{
	/*
	 * pass on each reference, update it or report error
	 * abnormal situations:
	 * a symbol is referenced but not defined or may not be referenced
	 * requesting the size of a non-code symbol
	 */
	Symbol *sym=(Symbol *)val;
	Ref *ref=sym->refs_head;
	/*find real symbol address, by adding the proper offset to it*/
	int sym_addr=sym->address+(sym->type==CODE?get_code_offset():get_data_offset());

	if(sym->is_entry)
		write_entry(label, sym_addr);

	while(ref)
	{
		if(ref->is_index && sym->type!=CODE)/*taking the size of a non code symbol*/
			set_error_line(ref->line_number, "the index symbol '%s' must point to valid code.", label);

		switch(sym->type)
		{
		case UNDEFINED:/*not defined*/
			set_error_line(ref->line_number, "reference to undefined symbol '%s'.", label);
			break;

		case EXTERN:
			/*write external reference for the linker*/
			write_extern(label, ref->address+get_code_offset());
			/*mark word for extern linking*/
			update_code(ref->address, WORD_ZERO, LM_EXTERN);
			break;

		case CODE:
			if(ref->is_index)/*symbol size is requested*/
				update_code(ref->address, word_from_int(sym->size), LM_ABS);
			else
				/*symbol address is requested. add code offset*/
				update_code(ref->address, word_from_int(sym_addr), LM_RELOC);
			break;

		case DATA:
			/*add data offset to symbol address*/
			update_code(ref->address, word_from_int(sym_addr), LM_RELOC);
			break;

		case OTHER:/*cannot be referenced*/
			set_error_line(ref->line_number, "cannot reference '%s' because it does not point to a valid code or data line.", label);
			break;
		}
		
		ref=ref->next;
	}

	return 1;
}
