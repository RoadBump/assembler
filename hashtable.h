/*
 * hashtable.h
 *
 *  Created on: 4\8\2014
 *      Author: Joseph Bruss
 */

#ifndef HASHTABLE_H_
#define HASHTABLE_H_

/*
 * A hash table entry
 */
typedef struct Entry {
	char *key;
	void *value;
	struct Entry *next;
} Entry;

/*
 * A generic hashtable
 */
typedef struct {
	int count;		/*count of items in table*/
	int size;		/*number of slots in table array*/
	Entry **array;
} Hashtable;

/*
 * Adds a key/value pair to the hash table. Fails if key already exists.
 * Returns whether the operation succeeded.
 */
int ht_add(Hashtable *ht, char *key, void *value);

/*
 * Removes a key/value pair from hash table.
 * Returns the value that has been removed, or NULL if not exist.
 * Does NOT free any memory used by value.
 */
void *ht_remove(Hashtable *ht, char *key);

/*
 * Returns the value that is associated with the key, or NULL if not exist.
 */
void *ht_get(Hashtable *ht, char* key);

/*
 * Returns whether the key appears in the table.
 */
int ht_exists(Hashtable *ht, char *key);

/*
 * Returns the number of objects stored in hash table.
 */
int ht_count(Hashtable *ht);

/*
 * Iterates over all values in table, calling a supplied function for each of them.
 * First parameter is the key of current item, second parameter is it's value.
 * Iteration continues as long as func returns non-zero.
 */
void ht_enumerate(Hashtable *ht, int (*func)(char*, void*));

/*
 * Removes all values from table.
 * If free_items is non zero, removed values's memory will be freed. Otherwise, it will not.
 */
void ht_clear(Hashtable *ht, int free_items);

#endif /* HASHTABLE_H_ */
