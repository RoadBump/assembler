/*
 * input-output.c
 * Handles reading and parsing from input file, and writing to output files.
 *
 *  Created on: 26/7/2014
 *      Author: Joseph Bruss
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdarg.h>
#include "input-output.h"

#define CODE_OFFSET 100
#define INIT_SIZE 256
#define OCT_DIGITS 7

#define EXT_INPUT ".as"
#define EXT_OUTPUT ".ob"
#define EXT_EXTERN ".ext"
#define EXT_ENTRIES ".ent"

#define MODE_READ "r"
#define MODE_WRITE "w"

#define SPACE 1
#define NON_SPACE 0
/*macro that appends ext to the end of file_name*/
#define append_extension(ext) strncat(file_name, (ext), FILENAME_MAX - strlen(file_name))

extern int grow_array(void **array_ptr, int size, int elt_size, int init_array);
static int tokenize_params(char *s, char *tokens[], int max_tokens);
static char *seek(char *s, int seek_space, char c);
static void stop_writing(void);
static void cleanup(void);
static void set_error_intern(int line_number, char* format, va_list args);

typedef struct {
	Word word;
	char link;
} CompiledWord;

static CompiledWord *code_array;
static Word *data_array;
static int code_counter, data_counter;
static int code_size, data_size;
static int line_num;
static char file_name[FILENAME_MAX + 1];
static FILE *input, *externals, *entries;
static int has_err;

/*
 * Reads next valid code line, splits it to tokens. Skips comment and blank lines. Checks for malformed lines.
 * Returns the number of tokens found, not including the label, or -1 on file error or end of file.
 * line- buffer to fill with characters read from file. All returned pointers point to within this buffer.
 * max_line- max line length
 * label- output parameter. Receives the label of the line, or NULL if not exist.
 * tokens- Array that is populated with all tokens in line, except the label. If tokens are malformed, remains empty.
 * max_tokens- size of array tokens.
 * length- output parameter. Receives the line length.
 */
int get_next_line(char *line, int max_line, char** label, char* tokens[], int max_tokens, int* length)
{
	char *lbl;
	int len, n_tokens;
	int found=0;
	while(!found && fgets(line, max_line, input))
	{
		char *b, *e;
		line_num++;
		/*newline always included if possible, line contains at least one character*/
		len=strlen(line);
		lbl=NULL;
		n_tokens=0;

		if(line[0] != ';'		/*not comment line*/
				&& line[len-1]=='\n' /*line entirely read*/
				&&*(b=seek(line, NON_SPACE, '\0'))!='\0') /*first token found, not blank line*/
		{
			/*b is at the beginning of first token, which is a label or operation name*/
			e=seek(b, SPACE, '\0');/*end of first token*/
			if(*(e-1)==':')/*label. necessarily e > b*/
			{
				*(e-1)='\0';/*terminate label, deleting ':'*/
				lbl=b;

				b=seek(e, NON_SPACE, '\0');/*seek operation name*/
			}

			if(*b && max_tokens > 0)
			{
				int n_params;

				tokens[n_tokens++]=b;/*operation name*/
				e=seek(b, SPACE, '\0');/*end of operation name*/
				*e='\0';/*terminate operation name*/

				n_params=tokenize_params(e+1, &tokens[1], max_tokens-1);
				if(n_params>=0)
					n_tokens+=n_params;
				else/*malformed parameters*/
					n_tokens=0;
			}

			found=(lbl!=NULL || n_tokens>0);
		}
		else if(line[len-1] != '\n')/*line was not read entirely*/
			set_error("too long line. lines may not be longer than %d.", max_line-1);
	}

	if(found)
	{
		*length=len;
		*label=lbl;
	}
	else/*eof or file error*/
	{
		if(ferror(input))
			set_error("error occurred while reading file '%s%s'.", file_name, EXT_INPUT);
		n_tokens=-1;
		*length=0;
		*label=NULL;
	}

	return n_tokens;
}

/*
 * Tokenize a list of parameters, separated by commas. trim white spaces.
 * double quotes after a white space are the beginning of a string,
 * which terminates with double quotes too.
 * Returns the number of parameters read, or -1 on format error.
 */
static int tokenize_params(char *s, char *tokens[], int max_tokens)
{
	char *b, *e;
	int seek_comma;
	int n_tokens=0;
	/*
	* b is always at beginning of parameter or at a comma separator
	* e is at the end of previous parameter
	* seek_comma holds whether we look for a comma of for a parameter
	*/
	b=seek(s, NON_SPACE, ',');/*first parameter*/
	e=NULL;
	seek_comma=0;

	while(*b && n_tokens < max_tokens)
	{
		if((*b==',') == seek_comma)/*we found what we expected*/
		{
			if(*b==',')
			{
				b=seek(b+1, NON_SPACE, ',');/*next parameter*/
				seek_comma=0;
			}
			else
			{
				tokens[n_tokens++]=b;
				if(e)
					*e='\0';/*terminate previous parameter*/
				if(*b=='\"')/*string, seek next " */
				{
					if((e=strchr(b+1, '\"')))
						e++;/*include qoutes*/
					else
						e=&s[strlen(s)];/*null terminator*/
				}
				else
					e=seek(b, SPACE, ',');/*end of parameter*/
				b=seek(e, NON_SPACE, ',');/*next comma separator*/
				seek_comma=1;
			}
		}
		else
		{
			if(seek_comma)
				set_error("missing comma separator.");
			else
				set_error("unexpected comma.");
			n_tokens=-1;
			break;
		}
	}

	if(e)/*terminate last parameter*/
		*e='\0';
	return n_tokens;
}

/*
 * Returns first occurrence in s of any of the following:
 * if seek_space is true, a white space. else, a non white space character.
 * character specified in c
 * string null terminator
 */
static char *seek(char *s, int seek_space, char c)
{
	while(*s
			&& *s != c
			&& (seek_space==0) != (isspace(*s)==0))
		s++;

	return s;
}

/*
 * Writes the formatted error message to stderr, and associates it with the current text line
 * Stops further writing to output.
 */
void set_error(char* format,...)
{
	va_list args;
	va_start(args, format);
	set_error_intern(get_line_number(), format, args);
	va_end(args);
}

/*
 * Writes the formatted error message to stderr, and associates it with given line.
 * Stops further writing to output.
 * Set line_number to zero to mark general errors.
 */
void set_error_line(int line_number, char* format, ...)
{
	va_list args;
	va_start(args, format);
	set_error_intern(line_number, format, args);
	va_end(args);
}

/*does the job for the set_error methods*/
static void set_error_intern(int line_number, char* format, va_list args)
{
	if(!has_error())
	{
		fprintf(stderr, "Errors in file '%s':\n", file_name);
		has_err= 1;
		stop_writing();
	}

	if(line_number!=0)
		fprintf(stderr, "[line %d]: ", line_number);
	else
		fprintf(stderr, "\t");
	vfprintf(stderr, format, args);
	fprintf(stderr, "\n");
}

/*
 * Returns whether an error occurred during current file compilation.
 */
int has_error(void)
{
	return has_err;
}

/*
 * Writes code and link mode to the end code block.
 */
void write_code(Word word, char link_mode)
{
	if(!has_error())
	{
		if(code_counter == code_size)
				code_size = grow_array((void **)&code_array, code_size, sizeof(CompiledWord), INIT_SIZE);
		code_array[code_counter].word = word;
		code_array[code_counter].link = link_mode;
		code_counter++;
	}
}

/*
 * Updates code block at given address. Address must correspond to that returned by get_code_counter().
 */
void update_code(int address, Word word, char link_mode)
{
	if(address>=0 && address < code_size)
	{
		code_array[address].word = word;
		code_array[address].link = link_mode;
	}
}

/*
 * Writes data to the end of data block.
 */
void write_data(Word data)
{
	if(!has_error())
	{
		if(data_counter == data_size)
			data_size = grow_array((void **)&data_array, data_size, sizeof(Word), INIT_SIZE);
		data_array[data_counter] = data;
		data_counter++;
	}
}

/*
 * Writes symbol and address to externals file.
 */
void write_extern(char* symbol, int address)
{
	if(!has_error())
	{
		if(externals == NULL)
		{
			int len=strlen(file_name);
			append_extension(EXT_EXTERN);
			externals=fopen(file_name, MODE_WRITE);
			if(!externals)
				set_error_line(0, "error creating file '%s'", file_name);
			file_name[len]='\0';/*discard extension*/
		}

		if(externals)
			fprintf(externals, "%s\t%o\n", symbol, address);
	}
}

/*
 * Writes symbol and address to entries file.
 */
void write_entry(char* symbol, int address)
{
	if(!has_error())
	{
		if(entries == NULL)
		{
			int len = strlen(file_name);
			append_extension(EXT_ENTRIES);
			entries = fopen(file_name, MODE_WRITE);
			if(!entries)
				set_error_line(0, "error creating file '%s'", file_name);
			file_name[len]='\0';/*discard extension*/
		}

		if(entries)
			fprintf(entries, "%s\t%o\n", symbol, address);
	}
}

/*
 * Begins input and output session for the specified file path.
 * Returns whether the file has opened successfully.
 */
int begin_source_file(char* file)
{
	int len=strlen(file);
	int ret=0;

	if(len + strlen(EXT_EXTERN) <= FILENAME_MAX)
	{
		cleanup();
		strcpy(file_name, file);
		append_extension(EXT_INPUT);
		input=fopen(file_name, MODE_READ);
		if(input==NULL)
		{
			fprintf(stderr, "error reading file '%s'.\n", file_name);
			cleanup();
		}
		else
			ret=1;

		file_name[len]='\0';/*get rid of extension*/
	}
	else
		fprintf(stderr, "too long file name: '%s'.\n", file);
	return ret;
}

/*
 * Flushes all output to files, finishing input/output session.
 */
void flush(void)
{
	if(!has_error())
	{
		FILE *output;
		append_extension(EXT_OUTPUT);
		output=fopen(file_name, MODE_WRITE);
		if(output)/*print output*/
		{
			int i;
			char s[OCT_DIGITS + 1];
			int address=CODE_OFFSET;
			/*first line: <code counter> <data counter> */
			fprintf(output, "%o %o\n", code_counter, data_counter);
			/*code block, word per line. format: <address> <7 digit octal machine word> <link mode> */
			for(i=0; i<code_counter; i++)
			{
				word_print(s, &(code_array[i].word));
				fprintf(output, "%o\t%s\t%c\n",address, s, code_array[i].link);
				address++;
			}
			/*data block, word per line*/
			for(i=0; i<data_counter; i++)
			{
				word_print(s, &data_array[i]);
				fprintf(output, "%o\t%s\n", address, s);
				address++;
			}

			fclose(output);
		}
		else
			set_error_line(0, "error creating file '%s'.", file_name);
	}

	cleanup();
}

/*Releases resources needed for writing output. Keeps input stream, error information and file name.*/
static void stop_writing(void)
{
	int len=strlen(file_name);
	if(code_array)
		free(code_array);
	if(data_array)
		free(data_array);
	code_array=NULL;
	data_array=NULL;
	code_counter=data_counter=code_size=data_size=0;
	if(externals)
	{
		fclose(externals);
		if(has_error())/*delete file*/
		{
			append_extension(EXT_EXTERN);
			remove(file_name);
			file_name[len]='\0';/*discard extension*/
		}
	}
	if(entries)
	{
		fclose(entries);
		if(has_error())/*delete file*/
		{
			append_extension(EXT_ENTRIES);
			remove(file_name);
			file_name[len]='\0';/*discard extension*/
		}
	}
	externals=entries=NULL;
}

/*Clears all data and releases all resources*/
static void cleanup(void)
{
	stop_writing();
	if(input)
		fclose(input);
	input=NULL;
	has_err=0;
	file_name[0]='\0';
}

/*
 *Returns the code counter
 */
int get_code_counter(void)
{
	return code_counter;
}

/*
 * Returns the data counter
 */
int get_data_counter(void)
{
	return data_counter;
}

/*
 * Returns current line number
 */
int get_line_number(void)
{
	return line_num;
}

/*
 * Returns offset of code block in memory
 */
int get_code_offset(void)
{
	return CODE_OFFSET;
}

/*
 * Returns offset of data block in memory
 */
int get_data_offset(void)
{
	/*data goes just after the code block*/
	return CODE_OFFSET + code_counter;
}
