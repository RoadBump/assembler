/*
 * main.c
 * Entry point to program
 *
 *  Created on: 5 /8/2014
 *      Author: Joseph Bruss
 */
#define RELEASE
#ifdef RELEASE
int main(int argc, char *argv[])
{
	extern int compile(char *);
	int ret=0;

	while(--argc > 0)
		if(!compile(*++argv))
			ret=1;

	return ret;
}
#endif

