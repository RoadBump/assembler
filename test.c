
#ifdef DEBUG
/*testing*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"
#include "word.h"
#include "symbol-table.h"

static void test_hashtable(void);
static int print_table(char *key, void *val);
static void test_word(void);
static void test_symbol_table(void);

int main(void)
{
	test_hashtable();
	test_word();
	test_symbol_table();
	return 0;
}

/*hashtable test*/
static void test_hashtable(void)
{
#define COUNT (sizeof list / sizeof list[0])
	struct {
		char name[20];
		int val;
		} list[]=
	{
		{"abc", 1},
		{"abb", 2},
		{"ab", 3},
		{"bb", 4},
		{"abcbc", 5}
	};
	Hashtable ht={0};
	int i;

	printf("test for hashtable\n");

	for(i=0; i< COUNT; i++)
		if(!ht_add(&ht, list[i].name, &list[i].val))
			printf("can't add %s %d\n", list[i].name, list[i].val);

	ht_enumerate(&ht, print_table);

	if(ht_count(&ht) != COUNT)
		printf("worng item count, expected: %d, actual: %d\n",COUNT, ht_count(&ht));

	for(i=0; i< COUNT; i++)
	{
		char *name=list[i].name;
		int *dat;
		if(!ht_exists(&ht, name))
			printf("key %s doesnt exist\n", name);
		if(ht_add(&ht, name, NULL))
			printf("duplicate key added: %s\n", name);
		dat=(int *)ht_get(&ht, name);
		if(!dat || *dat != list[i].val)
			printf("wrong value keyed under %s, expected %d\n", name, list[i].val);
	}

	ht_clear(&ht, 0);
	if(ht_count(&ht)!=0)
		printf("wrong item count, expected 0, actual %d\n", ht_count(&ht));	
}

static int print_table(char *key, void *val)
{
	printf("[%s]:%d\n", key, *(int *)val);
	return 1;
}

static void test_word(void)
{
#define MASK 0xFFFFF
	char *s[]={"0", "524287", "+0", "-0", "+123", "-123","-9873"};
	Word w;
	unsigned long n;
	int i;

	printf("test word\n");	
	
	for(i=0; i<(sizeof s / sizeof s[0]); i++)
	{
		if(!word_parse(s[i], &w))
			printf("parsing '%s' caused %s\n",s[i],(word_equals(w,WORD_MAX)?"overflow":"format error"));
		else
		{
			n=(unsigned long)atol(s[i]) & MASK;			
			if(w!=n)
				printf("'%s' parsed worng to '%lo', expected '%lo'", s[i], (unsigned long)w, n);
			else
			{
				char s1[20], s2[20];
				word_print(s1,&w);
				sprintf(s2,"%07lo",n);
				if(strcmp(s1,s2)!=0)
					printf("wrong print, expected: '%s' actual: '%s'\n", s2, s1);
			}
		}
	}

	for(i=-500; i<500; i+=98)
	{
		n=(unsigned long)i & MASK;
		w=word_from_int(i);
		if(w!=n)
			printf("word_from_int: expected: '%lu' actual: '%lu'\n",n,(unsigned long)w);
	}

	for(i=0;i<18;i++)
	{
		w=0;
		n=3<<i;
		word_set_bits(&w, i, 2, 3);
		if(w!=n)
			printf("word_set_bits: expected: '%lu' actual: '%lu'\n", n, (unsigned long)w);
		else if((n=word_get_bits(&w, i, 2))!=3)
			printf("word_get_bits: expected: '%d' actual: '%lu'\n", 3, n);
	}

	
}

static void test_symbol_table(void)
{
	printf("test symbol table\n");
	add_ref("normal", 0, 0, 0);
	add_symbol("normal", 0, 0, CODE);

	/*those are expected to produce errors*/
	add_symbol("normal", 0, 0, CODE);
	add_symbol("normal", 0, 0, OTHER);
	add_symbol("normal", 0, 0, DATA);
	add_extern("normal");

	add_entry("normal");/*OK*/

	add_extern("extern");
	add_ref("extern", 0, 0, 0);
	add_extern("extern");
	/*FAIL*/
	add_entry("extern");
	add_symbol("extern", 0, 0, OTHER);

	add_entry("entry");
	add_ref("entry", 0, 0, 0);
	add_entry("entry");
	/*FAIL*/
	add_extern("entry");
	add_symbol("entry", 0, 0, OTHER);

	add_symbol("entry", 0, 0, DATA);
}
#endif
