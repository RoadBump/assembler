/*
 * word.h
 *
 *  Created on: 4 /8/2014
 *      Author: Joseph Bruss
 */

#ifndef WORD_H_
#define WORD_H_

#include <limits.h>

#define WORD_BITS 20
#define WORD_MAX 01777777 /*biggest positive number*/
#define WORD_ZERO 0

/*20 bits signed machine word. Implemented using a big enough unsigned integer.
For implementation details refer to word.c*/
#if SHRT_MAX >= WORD_MAX
typedef unsigned short Word;
#elif INT_MAX >= WORD_MAX
typedef unsigned Word;
#else /*assume long is big enough*/
typedef unsigned long Word;
#endif

/*
 * Parse a string of decimal digits with an optional sign to a Word. Store result in res.
 * Returns whether the conversion succeeded.
 * If the conversion failed, res is assigned WORD_MAX on overflow error and WORD_ZERO otherwise.
 */
int word_parse(char *s, Word *res);

/*
 * Converts an integer to a Word. No check for overflow.
 */
Word word_from_int(int n);

/*
 * Prints the value of w in a seven-digit octal form into s, with leading zeros if necessary.
 */
void word_print(char *s, Word *w);

/*
 * Returns a specified bit sequence of w, right justified.
 * index: the starting index, from the right edge
 * count: the number of bits to copy from right to left
 * index, count and index+count must all be in range 0-19. Count must also fit into an unsigned integer.
 */
unsigned word_get_bits(Word *w, int index, int count);

/*
 * Sets bits in w to a specified bit pattern, given by bits.
 * index: the starting bit index in w, from the right edge
 * count: the number of bits to copy.
 * bits: the unsigned integer from where the 'count' rightmost bits are copied.
 * index, count and index+count must all be in range 0-19. Count must also fit into an unsigned integer.
 */
void word_set_bits(Word *w, int index, int count, unsigned bits);

/*
 * Returns whether x and y are equal.
 */
int word_equals(Word x, Word y);

#endif /* WORD_H_ */
